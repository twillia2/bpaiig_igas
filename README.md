# BPAIIG_iGAS

United Kingdom wide surveillance study examining paediatric invasive group A streptococcus cases in the 2022/23 season.

De-identified participant data that underlie the results reported in this study will be available by request to researchers who provide a methodologically sound proposal, until 2 years after publication. Requests would need to be approved by the consortium and should be directed to the corresponding author (emily.lees@paediatrics.ox.ac.uk).
